import React from "react";
import "../styles/ToRead.css";
import Card from "./Card";

const ToRead = ({ text, onDragOver, BookList, onDrop, onDragStart }) => {
  const renderCard = book => (
    <Card
      key={book.id}
      keyValue={book.id}
      bookName={book.bookname}
      bookAuthor={book.author}
      bookImage={book.image}
      onDragStart={onDragStart}
    />
  );
  return (
    <div
      className="to-read"
      onDragOver={e => onDragOver(e)}
      onDrop={e => onDrop(e, "toRead")}
    >
      <h2 className="task-header">{text}</h2>
      <div className="toRead-list">
        {BookList.filter(book => book.category === "toRead").map(book =>
          renderCard(book)
        )}
      </div>
    </div>
  );
};
export default ToRead;
