import React from "react";
import "../styles/CurrentlyReading.css";
import Card from "../components/Card";

const CurrentlyReading = ({
  text,
  onDragOver,
  BookList,
  onDrop,
  onDragStart
}) => {
  const renderCard = book => (
    <Card
      key={book.id}
      keyValue={book.id}
      bookName={book.bookname}
      bookAuthor={book.author}
      bookImage={book.image}
      onDragStart={onDragStart}
    />
  );
  return (
    <div
      className="current-read"
      onDragOver={e => onDragOver(e)}
      onDrop={e => onDrop(e, "current")}
    >
      <h2 className="task-header">{text}</h2>
      <div className="current-list">
        {BookList.filter(book => book.category === "current").map(book =>
          renderCard(book)
        )}
      </div>
    </div>
  );
};
export default CurrentlyReading;
