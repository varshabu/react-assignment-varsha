import React from "react";
import "../styles/AlreadyRead.css";
import Card from "../components/Card";

const AlreadyRead = ({ text, onDragOver, BookList, onDrop, onDragStart }) => {
  const renderCard = book => (
    <Card
      key={book.id}
      keyValue={book.id}
      bookName={book.bookname}
      bookAuthor={book.author}
      bookImage={book.image}
      onDragStart={onDragStart}
    />
  );
  return (
    <div
      className="already-read"
      onDragOver={e => onDragOver(e)}
      onDrop={e => onDrop(e, "read")}
    >
      <h2 className="task-header">{text}</h2>
      <div className="read-list">
        {BookList.filter(book => book.category === "read").map(book =>
          renderCard(book)
        )}
      </div>
    </div>
  );
};
export default AlreadyRead;
