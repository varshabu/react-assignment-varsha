import React, { Component } from "react";
import "../styles/Card.css";

export class Card extends Component {
  render() {
    const style = {
      backgroundImage: "url(" + this.props.bookImage + ")"
    };
    return (
      <div
        key={this.props.keyValue}
        onDragStart={e => this.props.onDragStart(e, this.props.keyValue)}
        draggable
        className="book-card"
      >
        <header style={style} className="image-header">
          {/* <h4>{bookAuthor}</h4> */}
        </header>
        <div className="card-body">
          <p>
            <strong>{this.props.bookName}</strong>
          </p>
          <h6>by {this.props.bookAuthor}</h6>
        </div>
      </div>
    );
  }
}

export default Card;
