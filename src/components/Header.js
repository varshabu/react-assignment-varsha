import React from "react";
import "../styles/Header.css";

import PropTypes from "prop-types";

const Header = props => (
  <nav>
    <div>
      <h1 className="header-title">{props.title}</h1>
    </div>
  </nav>
);

Header.propTypes = {
  title: PropTypes.string
};

export default Header;
