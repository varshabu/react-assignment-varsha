import React from "react";
import "../styles/Button.css";

const Button = props => (
  <div>
    <button className="button" onClick={props.handleClick}>
      Add New Book
    </button>
  </div>
);
export default Button;
