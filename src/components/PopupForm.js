import React, { Component } from "react";
import "../styles/PopupForm.css";
import uuid from "uuid";

export class PopupForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formData: {
        id: uuid.v4(),
        bookname: "",
        author: "",
        isbn: "",
        image: "",
        category: "toRead"
      }
    };
  }

  handleInputChange = e => {
    let formData = Object.assign({}, this.state.formData);
    formData[e.target.name] = e.target.value;
    this.setState({ formData });
  };

  addToBooks = e => {
    e.preventDefault();
    this.props.handleAddToBooks(this.state.formData);
  };

  render() {
    return (
      <div className="popup">
        <div className="popup-inner">
          <h1 className="form-header">{this.props.text}</h1>
          <form id="popupform" onSubmit={this.addToBooks}>
            <input
              className="input-fields"
              type="text"
              name="bookname"
              placeholder="Book Name"
              onChange={this.handleInputChange}
            />
            <input
              className="input-fields"
              type="text"
              name="author"
              placeholder="Author Name"
              onChange={this.handleInputChange}
            />
            <input
              className="input-fields"
              type="text"
              name="isbn"
              placeholder="ISBN"
              onChange={this.handleInputChange}
            />
            <input
              className="input-fields"
              type="url"
              name="image"
              placeholder="Image URL"
              onChange={this.handleInputChange}
            />
            <input className="submit-btn" type="submit" value="submit" />
          </form>
          <button className="close-btn" onClick={this.props.closePopup}>
            X
          </button>
        </div>
      </div>
    );
  }
}

export default PopupForm;
