import React, { Component } from "react";
import Header from "./components/Header";
import Button from "./components/Button";
import PopupForm from "./components/PopupForm";
import ToRead from "./components/ToRead";
import Current from "./components/CurrentlyReading";
import Read from "./components/AlreadyRead";
import db from "./DexieDB";

export class Home extends Component {
  constructor() {
    super();
    this.state = {
      title: "BookHouse",
      showPopup: false,
      books: []
    };
  }

  handleClick = () => {
    this.setState({
      showPopup: !this.state.showPopup
    });
  };

  handleAddToBooks = bookData => {
    console.log(bookData);
    const book = bookData;
    db.table("books")
      .add(book)
      .then(id => {
        const newList = [...this.state.books, Object.assign({}, book, { id })];
        this.setState({ books: newList });
      });
    const formReset = document.querySelector("#popupform");
    formReset.reset();
  };

  onDragStart = (event, id) => {
    event.dataTransfer.setData("id", id);
  };

  onDragOver = event => {
    event.preventDefault();
  };

  onDrop = (event, cat) => {
    let ids = event.dataTransfer.getData("id");
    let books = this.state.books.filter(book => {
      if (book.id === ids) {
        book.category = cat;
        db.books.update(book.id, { category: cat });
      }
      return book;
    });
    this.setState({
      ...this.state,
      books
    });
  };

  componentDidMount() {
    db.table("books")
      .toArray()
      .then(books => {
        this.setState({ books });
      });
  }
  render() {
    return (
      <div className="app-container">
        <div className="header-container">
          <Header title={this.state.title} />
          <Button handleClick={this.handleClick} />
        </div>
        <ToRead
          text="To Read"
          onDragStart={this.onDragStart}
          onDragOver={this.onDragOver}
          onDrop={this.onDrop}
          BookList={this.state.books}
        />
        <Current
          text="Currently Reading"
          onDragStart={this.onDragStart}
          onDragOver={this.onDragOver}
          onDrop={this.onDrop}
          BookList={this.state.books}
        />
        <Read
          text="Already Read"
          onDragStart={this.onDragStart}
          onDragOver={this.onDragOver}
          onDrop={this.onDrop}
          BookList={this.state.books}
        />
        {this.state.showPopup ? (
          <PopupForm
            text="Enter Book Details"
            handleAddToBooks={this.handleAddToBooks}
            closePopup={this.handleClick.bind(this)}
          />
        ) : null}
      </div>
    );
  }
}

export default Home;
