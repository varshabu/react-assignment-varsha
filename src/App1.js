import React, { Component } from "react";
import { BrowserRouter, Route } from "react-router-dom";
import Home from "./Home";
import "./App1.css";

export default class App1 extends Component {
  render() {
    return (
      <BrowserRouter>
        <Route path="/" exact component={Home} />
        {/* <Route path="/about" component={About} />*/}
      </BrowserRouter>
    );
  }
}
