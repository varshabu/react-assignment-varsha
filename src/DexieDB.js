import Dexie from "dexie";

const db = new Dexie("BooksDB");
db.version(1).stores({ books: "id" });

export default db;
